<?php
/**
 * Class Shopware_Controllers_Api_Articleimages
 */
class Shopware_Controllers_Api_Articleimages 
	extends Shopware_Controllers_Api_Rest
{
	/**
	 * @var Shopware\Components\Api\Resource\Property
	 */
	protected $resource;

	public function init() {
		$this->resource = \Shopware\Components\Api\Manager::getResource('image');
	}

	/**
	 * Update article/variants image
	 *
	 * PUT /api/articleimage/{id}
	 */
	public function putAction() {
		$imgId = $this->Request()->getParam('id');
		$params = $this->Request()->getPost();

		$image = $this->resource->update($imgId, $params);
		$this->View()->assign(['success' => true, 'data' => ['id' => $imgId, 'request' => $params, 'response' => $image]]);
	}

}
