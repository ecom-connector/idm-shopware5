<?php
/**
 * Class Shopware_Controllers_Api_Property
 */
class Shopware_Controllers_Api_Property 
	extends Shopware_Controllers_Api_Rest
{
	/**
	 * @var Shopware\Components\Api\Resource\Property
	 */
	protected $resource;

	public function init()
	{
		$this->resource = \Shopware\Components\Api\Manager::getResource('Property');
	}

	/**
	 * GET Request on /api/Property
	 */
	public function indexAction()
	{
		$result = $this->resource->getList();
		$this->View()->assign(['success' => true, 'data' => $result]);
	}


	/**
	 *
	 */
	public function getAction()
	{
		$id = $this->Request()->getParam('id');
		$propertyList = $this->resource->getOne($id);

		$this->View()->assign(['success' => true, 'property' => $id, 'data' => $propertyList]);
	}

	/**
	 *
	 */
	public function putAction()
	{
		$modelKey = $this->Request()->getParam('id');
		$params = $this->Request()->getPost();

		$property = $this->resource->update($modelKey, $params);
		$this->View()->assign(['success' => true, 'data' => ['request' => $params, 'response' => $property]]);
	}

}
