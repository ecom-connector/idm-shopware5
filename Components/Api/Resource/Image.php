<?php
namespace Shopware\Components\Api\Resource;

use Shopware\Components\Api\Exception as ApiException;

/**
 * Class Image
 *
 * @package Shopware\Components\Api\Resource
 */
class Image 
	extends Resource
{

	/**
	 * @param int   $id
	 * @param array $params
	 *
	 * @throws \Shopware\Components\Api\Exception\ValidationException
	 * @throws \Shopware\Components\Api\Exception\NotFoundException
	 * @throws \Shopware\Components\Api\Exception\ParameterMissingException
	 *
	 * @return \Shopware\Models\Article\Image
	 */
	public function update($id, array $params) {
		$this->checkPrivilege('update');
		
		if (empty($id) || !isset($params['mediaId'])) {
			throw new ApiException\ParameterMissingException();
		}
		
		if(isset($params['main']) && $params['main'] == 1) {
			// set all other images on main = 2
			$sql = "
				UPDATE	s_articles_img m
								LEFT JOIN s_articles_img c
									ON (m.id = c.parent_id OR  m.id = c.id)
				SET 		c.main = 2
				WHERE 		m.articleID = :articleId";
			$this->getManager()->getConnection()->executeUpdate( $sql, [':articleId' => $id] );
		}
		
		$builder = Shopware()->Container()->get('models')->createQueryBuilder();
		$builder->select(['image'])
						->from('Shopware\Models\Article\Image', 'image')
						->where('image.mediaId = :mediaId')
						->andWhere('image.article = :articleID')
						->setParameter('mediaId', $params['mediaId'])
						->setParameter('articleID', $id);
		$image = $builder->getQuery()->getOneOrNullResult( self::HYDRATE_OBJECT );		
		if (!$image) {
			throw new ApiException\NotFoundException("Image by id " . $params['mediaId'] . " not found");
		}

		unset($params['mediaId']);
		$image->fromArray($params);
		$violations = $this->getManager()->validate($image);
		if ($violations->count() > 0) {
			throw new ApiException\ValidationException($violations);
		}
		
		$this->getManager()->persist($image);
		$this->flush();
		
		return ['image' => $this->getManager()->toArray($image)];
	}

}
