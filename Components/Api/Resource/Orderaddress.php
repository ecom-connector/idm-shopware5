<?php
namespace Shopware\Components\Api\Resource;

use Shopware\Components\Api\Exception as ApiException;

/**
 *
 * @package Shopware\Components\Api\Resource
 */
class Orderaddress 
	extends Resource
{

	/**
	* 
	*/
	public function getList($orderId, $customerId)
	{
		$list = [];
		$countries = [];
		
		foreach(['Billing', 'Shipping'] as $addressKind) {
			$sqlTable = strtolower($addressKind);
			$builder = Shopware()->Models()->createQueryBuilder();
			$sqlSelects = [
				$sqlTable.'.company',
				$sqlTable.'.department',
				$sqlTable.'.salutation',
				$sqlTable.'.title',
				$sqlTable.'.firstName',
				$sqlTable.'.lastName',
				$sqlTable.'.street',
				$sqlTable.'.zipCode',
				$sqlTable.'.city',
				$sqlTable.'.phone',
				$sqlTable.'.countryId',
//			'billing.state',
				$sqlTable.'.additionalAddressLine1',
				$sqlTable.'.additionalAddressLine2',
			];
			if($addressKind == 'Billing') 
				$sqlSelects[] = $sqlTable.'.vatId';
				
			$builder->select($sqlSelects);

			$builder->from("Shopware\\Models\\Order\\".$addressKind, $sqlTable)
							->where('IDENTITY('.$sqlTable.'.customer) = :userId AND IDENTITY('.$sqlTable.'.order) = :orderId')
							->setParameters([
								'userId' => $customerId,
								'orderId' => $orderId,
							]);
							
			$query = $builder->getQuery();
			//return $query->getSql();
			
			$query->setHydrationMode($this->resultMode);
			$paginator = $this->getManager()->createPaginator($query);

			$totalResult = $paginator->count();
			$data = $paginator->getIterator()->getArrayCopy();

			foreach($data as &$dataItem) {
				if(!empty($dataItem['countryId'])) {
					if(!isset($countries[$dataItem['countryId']])) {
						$builder = $this->getManager()->getRepository('Shopware\\Models\\Country\\Country')->getCountryQueryBuilder($dataItem['countryId']);
						$query = $builder->getQuery();
						$query->setHydrationMode($this->resultMode);
						$paginator = $this->getManager()->createPaginator($query);
						$countries[$dataItem['countryId']] = $paginator->getIterator()->getArrayCopy()[0];
					}
					$dataItem['country'] = $countries[$dataItem['countryId']];
				}
			}
			
			$list[$addressKind] = [
				'count' => $totalResult,
				'data' => $data
			];
		}
		return $list;
	}

}
