<?php
namespace Shopware\Components\Api\Resource;

use Shopware\Components\Api\Exception as ApiException;

/**
 *
 * @package Shopware\Components\Api\Resource
 */
class Orderproduct 
	extends Resource
{

	/**
	* 
	*/
	public function getList($orderId, $customerId)
	{
		$list = [];
		$builder = Shopware()->Models()->createQueryBuilder();
		
		$sqlTable = 'detail';
		$sqlSelects = [
			$sqlTable.'.number',
			$sqlTable.'.articleId',
			$sqlTable.'.articleNumber',
			$sqlTable.'.articleName',
			$sqlTable.'.price',
			$sqlTable.'.taxId',
			$sqlTable.'.taxRate',
			$sqlTable.'.quantity',
/*			
	`status` INT(1) NOT NULL DEFAULT '0',
	`shipped` INT(11) NOT NULL DEFAULT '0',
	`shippedgroup` INT(11) NOT NULL DEFAULT '0',
	`releasedate` DATE NULL DEFAULT NULL,
	`modus` INT(11) NOT NULL,
	`esdarticle` INT(1) NOT NULL,
	`ean` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`unit` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
	`pack_unit` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8_unicode_ci',
*/
		];
		$builder->select($sqlSelects);

		$builder->from("Shopware\\Models\\Order\\Detail", $sqlTable)
						->where('IDENTITY('.$sqlTable.'.order) = :orderId')
						->setParameters([
							'orderId' => $orderId
						]);
						
		$query = $builder->getQuery();
//		return $query->getSql();
		
		$query->setHydrationMode($this->resultMode);
		$paginator = $this->getManager()->createPaginator($query);
		
		$totalResult = $paginator->count();
		$data = $paginator->getIterator()->getArrayCopy()[0];
		
		// product
		$sqlSelects = [
			$sqlTable.'.shippingTime',
		];
		$builder = Shopware()->Models()->createQueryBuilder();
		$builder->select($sqlSelects)
						->from("Shopware\\Models\\Article\\Detail", $sqlTable)
						->where('IDENTITY('.$sqlTable.'.article) = :articleId')
						->setParameters([
							'articleId' => $data['articleId']
						]);
		$query = $builder->getQuery();
		$query->setHydrationMode($this->resultMode);
		$paginator = $this->getManager()->createPaginator($query);
		$data = array_merge($data, $paginator->getIterator()->getArrayCopy()[0]);
		
		$list['products'] = [
			'count' => $totalResult,
			'data' => [$data]
		];
		
		return $list;
	}

}
