<?php
namespace Shopware\Components\Api\Resource;

use Shopware\Components\Api\Exception as ApiException;

/**
 * Class Property
 *
 * @package Shopware\Components\Api\Resource
 */
class Property 
	extends Resource
{
	
	private $namespaces = [
		'tax' => ['Tax', 'Tax'],
		'payment' => ['Payment', 'Payment'],
		'shipping' => ['Dispatch', 'Dispatch'],
		'manufacture' => ['Article', 'Supplier'],
		'unit' => ['Article', 'Unit'],
		'orderStatus' => ['Order', 'Status'],
		'paymentStatus' => ['Order', 'Status'],
		'customerStatus' => ['Customer', 'Group'],
		'configuratorOptions' => ['Article', 'Configurator', 'Option'],
	];
	
	/**
	 * get requested model Repository
	 */
	public function getRepository($modelName) {
		return $this->getManager()->getRepository('Shopware\\Models\\'.$this->namespaces[$modelName][0].'\\'.$this->namespaces[$modelName][0]);
	}


	/**
	 * @return array List of available properties
	 */
	public function getList()
	{
		$posibleProperties = [
			'tax',
			'payment',
			'shipping'
		];

		return ['data' => $posibleProperties, 'total' => count($posibleProperties) ];
	}

	/**
	 * Get List of asked Property
	 *
	 * @param $id
	 * @return mixed
	 * @throws ApiException\NotFoundException
	 * @throws ApiException\ParameterMissingException
	 */
	public function getOne($id)
	{
		if($id == 'orderStatus')
			$builder = $this->getRepository($id)->getOrderStatusQueryBuilder();

		else if($id == 'paymentStatus')
			$builder = $this->getRepository($id)->getPaymentStatusQueryBuilder();
		
		else if($id == 'customerStatus')
			$builder = $this->getRepository($id)->getCustomerGroupsQueryBuilder();

		else if($id == 'manufacture')
			$builder = $this->getRepository($id)->getSupplierListQueryBuilder(null, []);
		
		else if($id == 'unit')
			$builder = $this->getRepository($id)->getUnitsQueryBuilder(null, []);

		else if($id == 'configuratorOptions')
			$builder = $this->getRepository($id)->getAllConfiguratorOptionsIndexedByIdQueryBuilder();
		
		else if($id == 'articlesAttribute') {
        $builder = Shopware()->Container()->get('models')->createQueryBuilder();
        $builder
						->select(['configuration'])
            ->from('Shopware\Models\Attribute\Configuration', 'configuration')
            ->where('configuration.tableName = ?1')
            ->setParameter(1, 's_articles_attributes');
		}
		
		else if($id == 'versions') {
        $builder = Shopware()->Container()->get('models')->createQueryBuilder();
        $builder
						->select(['version'])
            ->from('Shopware\Models\Plugin\Plugin', 'version');
		}
		
		else if(in_array($id, $this->getList()['data'])) 
			$builder = $this->getRepository($id)->createQueryBuilder($id);
		
		else
			return null;
		
		$query = $builder->getQuery();
		$query->setHydrationMode($this->resultMode);

		$paginator = $this->getManager()->createPaginator($query);

		//returns the total count of the query
		$totalResult = $paginator->count();

		//returns the Model data
		$list = $paginator->getIterator()->getArrayCopy();

		return $list;
	}
	
		/**
     * @param $id
     * @param array $params
     * @return null|object
     * @throws ApiException\ValidationException
     * @throws ApiException\NotFoundException
     * @throws ApiException\ParameterMissingException
     */
    public function update($modelKey, array $params)
    {
				$changeLogger = [];
        $this->checkPrivilege('update');

        if (empty($params['id'])) {
            throw new ApiException\ParameterMissingException();
        }
				$modelId = $params['id'];
				unset($params['id']);

        $builder = Shopware()->Container()->get('models')->createQueryBuilder();
        $builder
						->select(['models'])
            ->from('Shopware\\Models\\' . implode('\\', $this->namespaces[$modelKey]), 'models')
            ->where('models.id = :modelId')
            ->setParameter('modelId', $modelId);

				$query = $builder->getQuery();
				$query->setHydrationMode($this->resultMode);
				
        $model = $query->getResult()[0];
				//$arrModel = $query->getArrayResult();

        if (!$model) {
            throw new ApiException\NotFoundException("model by id $id not found");
        }
				
				foreach($params as $k => $v) {
					$k = str_replace('_', '', lcfirst(ucwords($k, '_'))); // camalize
					$method = 'set'.ucfirst($k);
					if (is_callable([$model, $method])) {
						$model->$method($v);
						$changeLogger[$k] = $v;
					}
				}
					
				Shopware()->Models()->persist($model);
				Shopware()->Models()->flush();				

				return json_encode(['modelId' => $modelId, 'changeValues' => $changeLogger]);
    }	

}
