Das Plugin dient als Erweiterung zur on-board REST API von Shopware v5.

Es werden zuusätzlich benötigte Informationen, i.d.R. Meta Daten, vom shop abgefragt und geändert.

### Version ###

* Version: 1.1.x
* getestet in Shopware: 5.x

### Plugin in Magento implementieren ###

* Installation im Verzeichnis [MAGENTO_ROOT]\custom\plugins\ItdimeConnectorApi\

### Plugin über composer ###	

**Hierzu muss SW5 als Composer Version installiert sein** (https://github.com/shopware/composer-project).
Dann kann das normal im Composer eingebunden werden: 
```
require {
    ecom-connector/idm-shopware5 
}
```